fetch('/read')
.then(response => response.json())
.then(data => {
  mainNode = document.getElementById("div1")
  for (const x of data) {
    let todo = document.createElement("p")
    let todo_text = document.createTextNode(x[1])
    todo.appendChild(todo_text)
    mainNode.appendChild(todo)
  }
}).catch((err => {
   console.log(err)
});

let toDoForm = document.getElementById("todoform")
toDoForm.addEventListener('submit', function(event){
  event.preventDefault()
  let todo = document.getElementById("todoname")
  let data = new URLSearchParams()
  data.append('todovalue', todo.value)
  fetch('/create', {
    method: "POST",
    body: data
  }).then(function(response) {
    location.reload()
  }).catch(function(error){
    console.log(error)
  })
})
