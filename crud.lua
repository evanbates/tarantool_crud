--this is the "operations" crud.lua that goes in usr/share/tarantool

function create(req)
  local selfVar = req:post_param(data)
  for k,v in pairs(selfVar) do
    box.space.crudspace:auto_increment{v}
  end
end

function read(self)
  return self:render({ json = box.space.crudspace:select() })
end

function start()
  local server = require('http.server').new(nil, 8080, {app_dir='/usr/share/tarantool'})
  server:route({ path = '/', file='index.html'})
  server:route({ path = '/read'}, read)
  server:route({ path = '/create'}, create)
  server:start()
end

return {
  start = start
}

